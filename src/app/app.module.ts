import {BrowserModule} from "@angular/platform-browser";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";
import {HttpModule} from "@angular/http";
import {AppComponent} from "./app.component";
import {SceneModule} from "./scene/scene.module";
import {ConfigExporter} from "./exporter/config.exporter";
import {ConfigSceneMapper} from "./exporter/mapper/config.scene.mapper";
import {ConfigElementsMapper} from "./exporter/mapper/config.elements.mapper";
import {EngineMapper} from "./exporter/mapper/engine.mapper";
import {SimulationMapper} from "./exporter/mapper/simulation.mapper";

@NgModule({
  imports: [
    SceneModule,
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [ConfigExporter, ConfigSceneMapper, ConfigElementsMapper, EngineMapper, SimulationMapper],
  bootstrap: [AppComponent]
})
export class AppModule { }
