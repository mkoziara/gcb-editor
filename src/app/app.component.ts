import {Component, OnInit} from "@angular/core";
import {Scene} from "./scene/scene";
import {ImageElement} from "./scene/elements/image/image.element";
import {SceneElements} from "./scene/scene.elements";
import {ConfigExporter} from "./exporter/config.exporter";
import {Engine} from "./model/engine.model";
import {Simulation} from "./model/simulation.model";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'GCB editor';
  scenes: Array<Scene>;
  selectedScene: Scene;

  ngOnInit(): void {
    this.scenes = this.getScenes();
    this.selectedScene = this.scenes[0];
  }

  selectScene(scene: Scene): void {
    this.selectedScene = scene;
  }

  exportConfig(): void {
    let config = this.createSimulation();
    this.configExporter.exportConfig(config);
  }

  constructor(private configExporter: ConfigExporter) {
  }

  private createSimulation(): Simulation {
    let engine = this.createEngine();
    return new Simulation("gcb_ls_m_271_s01", "Vertical and supplementary angles", "interactive-board-engine", "interactive-board", engine);
  }

  private createEngine(): Engine {
    return new Engine(1920, 1080, true, true, [], this.scenes);
  }


  private getScenes(): Array<Scene> {
    return [
      new Scene("maciek1", "test.jpg", "red", new SceneElements([this.createImageElement()]), [], []),
      new Scene("kozik", "", "blue", new SceneElements([]), [], []),
      new Scene("teścik", "test.jpg", "green", new SceneElements([]), [], [])
    ]
  }

  private createImageElement(): ImageElement {
    return new ImageElement("image-test", "", "", 0, true, 550, 150, 350, 250, "/assets/winner.jpg", "desc");
  }
}
