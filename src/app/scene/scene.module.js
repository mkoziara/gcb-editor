"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var scene_component_1 = require("./scene.component");
var image_component_1 = require("./elements/image/image.component");
var common_1 = require("@angular/common");
var SceneModule = (function () {
    function SceneModule() {
    }
    SceneModule = __decorate([
        core_1.NgModule({
            imports: [common_1.CommonModule],
            declarations: [scene_component_1.SceneComponent, image_component_1.ImageComponent],
            exports: [scene_component_1.SceneComponent]
        })
    ], SceneModule);
    return SceneModule;
}());
exports.SceneModule = SceneModule;
