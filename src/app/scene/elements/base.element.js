"use strict";
var BaseElement = (function () {
    function BaseElement(elementId, style, // ??
        cssClass, // ??
        index, // ??
        visible, // ??
        positionX, positionY, width, height) {
        this.elementId = elementId;
        this.style = style;
        this.cssClass = cssClass;
        this.index = index;
        this.visible = visible;
        this.positionX = positionX;
        this.positionY = positionY;
        this.width = width;
        this.height = height;
    }
    return BaseElement;
}());
exports.BaseElement = BaseElement;
