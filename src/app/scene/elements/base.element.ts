export abstract class BaseElement {

  constructor(public readonly elementId: string,
              public readonly style: string,      // ??
              public readonly cssClass: string, // ??
              public readonly index: number,    // ??
              public readonly visible: boolean, // ??
              public readonly positionX: number,
              public readonly positionY: number,
              public readonly width: number,
              public readonly height: number) {
  }

  public abstract getType(): string;
}
