import {BaseElement} from "../base.element";

export class ImageElement extends BaseElement {

  public static readonly IMAGE_TYPE: string = "image";

  constructor(public readonly elementId: string,
              public readonly style: string,
              public readonly className: string,
              public readonly index: number,
              public readonly visible: boolean,
              public readonly positionX: number,
              public readonly positionY: number,
              public readonly width: number,
              public readonly height: number,
              public readonly imageSrc: string,
              public readonly alt: string) {
    super(elementId, style, className, index, visible, positionX, positionY, width, height);
  }

  getType(): string {
    return ImageElement.IMAGE_TYPE;
  }
}
