import {Component, Input} from "@angular/core";
import {ImageElement} from "./image.element";

@Component({
  selector: 'image-element',
  templateUrl: 'image.component.html',
  styleUrls: ['image.component.css']
})
export class ImageComponent {

  @Input()
  imageElement: ImageElement;


}
