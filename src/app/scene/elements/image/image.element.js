"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var base_element_1 = require("../base.element");
var ImageElement = (function (_super) {
    __extends(ImageElement, _super);
    function ImageElement(elementId, style, className, index, visible, positionX, positionY, width, height, imageSrc, alt) {
        _super.call(this, elementId, style, className, index, visible, positionX, positionY, width, height);
        this.elementId = elementId;
        this.style = style;
        this.className = className;
        this.index = index;
        this.visible = visible;
        this.positionX = positionX;
        this.positionY = positionY;
        this.width = width;
        this.height = height;
        this.imageSrc = imageSrc;
        this.alt = alt;
    }
    ImageElement.prototype.getType = function () {
        return ImageElement.IMAGE_TYPE;
    };
    ImageElement.IMAGE_TYPE = "image";
    return ImageElement;
}(base_element_1.BaseElement));
exports.ImageElement = ImageElement;
