"use strict";
var Scene = (function () {
    function Scene(name, // lub id
        backgroundImage, // konkretny problem z dodaniem tego stylu
        backgroundColor, elements, actions, logic) {
        this.name = name;
        this.backgroundImage = backgroundImage;
        this.backgroundColor = backgroundColor;
        this.elements = elements;
        this.actions = actions;
        this.logic = logic;
    }
    return Scene;
}());
exports.Scene = Scene;
