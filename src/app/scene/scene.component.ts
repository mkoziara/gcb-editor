import {Component, Input} from "@angular/core";
import {Scene} from "./scene";

@Component({
  selector: "scene",
  templateUrl: "scene.component.html",
  styleUrls: ["scene.component.css"]

})
export class SceneComponent {

  @Input()
  scene: Scene;

}
