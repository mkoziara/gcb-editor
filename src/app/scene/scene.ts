import {SceneElements} from "./scene.elements";

export class Scene {

  constructor(public readonly name: string, // lub id
              public readonly backgroundImage: string, // konkretny problem z dodaniem tego stylu
              public readonly backgroundColor: string,
              public readonly elements: SceneElements,
              public readonly actions: Array<any>,
              public readonly logic: Array<any>) {

  }
}
