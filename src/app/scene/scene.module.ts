import {NgModule} from "@angular/core";
import {SceneComponent} from "./scene.component";
import {ImageComponent} from "./elements/image/image.component";
import {CommonModule} from "@angular/common";

@NgModule({
  imports: [CommonModule],
  declarations: [SceneComponent, ImageComponent],
  exports: [SceneComponent]
})
export class SceneModule {

}
