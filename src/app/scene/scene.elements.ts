import {ImageElement} from "./elements/image/image.element";

export class SceneElements {

  constructor(public readonly imageElements: Array<ImageElement>) {
  }
}
