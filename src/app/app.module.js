"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var platform_browser_1 = require("@angular/platform-browser");
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var app_component_1 = require("./app.component");
var scene_module_1 = require("./scene/scene.module");
var config_exporter_1 = require("./exporter/config.exporter");
var config_scene_mapper_1 = require("./exporter/mapper/config.scene.mapper");
var config_elements_mapper_1 = require("./exporter/mapper/config.elements.mapper");
var engine_mapper_1 = require("./exporter/mapper/engine.mapper");
var simulation_mapper_1 = require("./exporter/mapper/simulation.mapper");
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        core_1.NgModule({
            imports: [
                scene_module_1.SceneModule,
                platform_browser_1.BrowserModule,
                forms_1.FormsModule,
                http_1.HttpModule
            ],
            declarations: [
                app_component_1.AppComponent
            ],
            providers: [config_exporter_1.ConfigExporter, config_scene_mapper_1.ConfigSceneMapper, config_elements_mapper_1.ConfigElementsMapper, engine_mapper_1.EngineMapper, simulation_mapper_1.SimulationMapper],
            bootstrap: [app_component_1.AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
exports.AppModule = AppModule;
