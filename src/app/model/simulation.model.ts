import {Engine} from "./engine.model";

export class Simulation {

  constructor(public readonly name: string,
              public readonly title: string,
              public readonly engineName: string,
              public readonly engineSkin: string,
              public readonly engine: Engine) {

  }
}
