import {Scene} from "../scene/scene";

export class Engine {

  constructor(public readonly width: number,
              public readonly height: number,
              public readonly autoresize: boolean,
              public readonly appendScenes: boolean,
              public readonly globalElements: Array<any>,
              public readonly scenes: Array<Scene>) {

  }

}
