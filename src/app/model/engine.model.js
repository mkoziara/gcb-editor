"use strict";
var Engine = (function () {
    function Engine(width, height, autoresize, appendScenes, globalElements, scenes) {
        this.width = width;
        this.height = height;
        this.autoresize = autoresize;
        this.appendScenes = appendScenes;
        this.globalElements = globalElements;
        this.scenes = scenes;
    }
    return Engine;
}());
exports.Engine = Engine;
