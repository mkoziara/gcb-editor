"use strict";
var Simulation = (function () {
    function Simulation(name, title, engineName, engineSkin, engine) {
        this.name = name;
        this.title = title;
        this.engineName = engineName;
        this.engineSkin = engineSkin;
        this.engine = engine;
    }
    return Simulation;
}());
exports.Simulation = Simulation;
