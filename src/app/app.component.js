"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var scene_1 = require("./scene/scene");
var image_element_1 = require("./scene/elements/image/image.element");
var scene_elements_1 = require("./scene/scene.elements");
var engine_model_1 = require("./model/engine.model");
var simulation_model_1 = require("./model/simulation.model");
var AppComponent = (function () {
    function AppComponent(configExporter) {
        this.configExporter = configExporter;
        this.title = 'GCB editor';
    }
    AppComponent.prototype.ngOnInit = function () {
        this.scenes = this.getScenes();
        this.selectedScene = this.scenes[0];
    };
    AppComponent.prototype.selectScene = function (scene) {
        this.selectedScene = scene;
    };
    AppComponent.prototype.exportConfig = function () {
        var config = this.createSimulation();
        this.configExporter.exportConfig(config);
    };
    AppComponent.prototype.createSimulation = function () {
        var engine = this.createEngine();
        return new simulation_model_1.Simulation("gcb_ls_m_271_s01", "Vertical and supplementary angles", "interactive-board-engine", "interactive-board", engine);
    };
    AppComponent.prototype.createEngine = function () {
        return new engine_model_1.Engine(1920, 1080, true, true, [], this.scenes);
    };
    AppComponent.prototype.getScenes = function () {
        return [
            new scene_1.Scene("maciek 1", "test.jpg", "red", new scene_elements_1.SceneElements([this.createImageElement()]), [], []),
            new scene_1.Scene("kozik", "", "blue", new scene_elements_1.SceneElements([]), [], []),
            new scene_1.Scene("teścik", "test.jpg", "green", new scene_elements_1.SceneElements([]), [], [])
        ];
    };
    AppComponent.prototype.createImageElement = function () {
        return new image_element_1.ImageElement("image-test", "", "", 0, true, 250, 150, 70, 65, "/assets/winner.jpg", "desc");
    };
    AppComponent = __decorate([
        core_1.Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        })
    ], AppComponent);
    return AppComponent;
}());
exports.AppComponent = AppComponent;
