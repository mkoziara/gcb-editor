import {Injectable} from "@angular/core";
import {ConfigScene} from "../model/config.scene.model";
import {Scene} from "../../scene/scene";
import {ConfigElementsMapper} from "./config.elements.mapper";

@Injectable()
export class ConfigSceneMapper {

  public mapToConfigScene(scene: Scene): ConfigScene {
    let configElements = this.configElementsMapper.mapToConfigElements(scene.elements);
    return new ConfigScene(scene.name, scene.backgroundImage, scene.backgroundColor, configElements, [], []);
  }

  constructor(private configElementsMapper: ConfigElementsMapper){}
}
