"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var config_engine_1 = require("../model/config.engine");
var EngineMapper = (function () {
    function EngineMapper(configSceneMapper) {
        this.configSceneMapper = configSceneMapper;
    }
    EngineMapper.prototype.mapToConfigEngine = function (engine) {
        var _this = this;
        var configScenes = engine.scenes.map(function (scene) { return _this.configSceneMapper.mapToConfigScene(scene); });
        return new config_engine_1.ConfigEngine(engine.width, engine.height, engine.autoresize, engine.appendScenes, engine.globalElements, configScenes);
    };
    EngineMapper = __decorate([
        core_1.Injectable()
    ], EngineMapper);
    return EngineMapper;
}());
exports.EngineMapper = EngineMapper;
