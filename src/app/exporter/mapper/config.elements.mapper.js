"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var config_image_element_1 = require("../model/elements/config.image.element");
var core_1 = require("@angular/core");
var ConfigElementsMapper = (function () {
    function ConfigElementsMapper() {
    }
    ConfigElementsMapper.prototype.mapToConfigElements = function (sceneElements) {
        return this.mapImageElements(sceneElements.imageElements);
    };
    ConfigElementsMapper.prototype.mapImageElements = function (imageElements) {
        return imageElements.map(function (v) {
            return new config_image_element_1.ConfigImageElement(v.elementId, v.getType(), v.style, v.cssClass, v.index, v.visible, v.positionX, v.positionY, v.width, v.height, v.imageSrc, v.alt);
        });
    };
    ConfigElementsMapper = __decorate([
        core_1.Injectable()
    ], ConfigElementsMapper);
    return ConfigElementsMapper;
}());
exports.ConfigElementsMapper = ConfigElementsMapper;
