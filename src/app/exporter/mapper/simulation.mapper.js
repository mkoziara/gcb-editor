"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var config_model_1 = require("../model/config.model");
var SimulationMapper = (function () {
    function SimulationMapper(engineMapper) {
        this.engineMapper = engineMapper;
    }
    SimulationMapper.prototype.mapToConfig = function (simulation) {
        var configEngine = this.engineMapper.mapToConfigEngine(simulation.engine);
        return new config_model_1.Config(simulation.name, simulation.title, simulation.engineName, simulation.engineSkin, configEngine);
    };
    SimulationMapper = __decorate([
        core_1.Injectable()
    ], SimulationMapper);
    return SimulationMapper;
}());
exports.SimulationMapper = SimulationMapper;
