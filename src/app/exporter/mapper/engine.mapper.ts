import {Injectable} from "@angular/core";
import {ConfigEngine} from "../model/config.engine";
import {Engine} from "../../model/engine.model";
import {ConfigSceneMapper} from "./config.scene.mapper";

@Injectable()
export class EngineMapper {

  public mapToConfigEngine(engine: Engine): ConfigEngine {

    let configScenes = engine.scenes.map(scene => this.configSceneMapper.mapToConfigScene(scene));
    return new ConfigEngine(engine.width, engine.height, engine.autoresize, engine.appendScenes, engine.globalElements, configScenes);
  }

  constructor(private configSceneMapper: ConfigSceneMapper) {
  }
}
