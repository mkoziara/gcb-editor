import {Injectable} from "@angular/core";
import {Config} from "../model/config.model";
import {Simulation} from "../../model/simulation.model";
import {EngineMapper} from "./engine.mapper";

@Injectable()
export class SimulationMapper {

  public mapToConfig(simulation: Simulation): Config {
    let configEngine = this.engineMapper.mapToConfigEngine(simulation.engine);
    return new Config(simulation.name, simulation.title, simulation.engineName, simulation.engineSkin, configEngine);
  }


  constructor(private engineMapper: EngineMapper) {
  }
}
