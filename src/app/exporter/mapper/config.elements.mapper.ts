import {SceneElements} from "../../scene/scene.elements";
import {ImageElement} from "../../scene/elements/image/image.element";
import {ConfigBaseElement} from "../model/elements/config.base.element";
import {Injectable} from "@angular/core";
import {ConfigImageContent} from "../model/elements/config.image.content";

@Injectable()
export class ConfigElementsMapper {

  public mapToConfigElements(sceneElements: SceneElements): Array<ConfigBaseElement> {
    return this.mapImageElements(sceneElements.imageElements);
  }


  private mapImageElements(imageElements: Array<ImageElement>): Array<ConfigBaseElement> {

    return imageElements.map((v) => {
      let imageContent = new ConfigImageContent(v.imageSrc, v.alt);

      return new ConfigBaseElement(v.elementId, v.getType(), v.style, v.cssClass,
        v.index, v.visible, v.positionX, v.positionY, v.width, v.height, imageContent);
    });
  }
}
