"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var config_scene_model_1 = require("../model/config.scene.model");
var ConfigSceneMapper = (function () {
    function ConfigSceneMapper(configElementsMapper) {
        this.configElementsMapper = configElementsMapper;
    }
    ConfigSceneMapper.prototype.mapToConfigScene = function (scene) {
        var configElements = this.configElementsMapper.mapToConfigElements(scene.elements);
        return new config_scene_model_1.ConfigScene(scene.name, scene.backgroundImage, scene.backgroundColor, configElements, [], []);
    };
    ConfigSceneMapper = __decorate([
        core_1.Injectable()
    ], ConfigSceneMapper);
    return ConfigSceneMapper;
}());
exports.ConfigSceneMapper = ConfigSceneMapper;
