import {serialize, serializeAs} from "cerialize";
import {ConfigElementContent} from "./config.element.content";

export class ConfigBaseElement {

  @serialize public readonly id;
  @serialize public readonly type;
  @serialize public readonly style;
  @serializeAs('class') public readonly cssClass;
  @serialize public readonly index;
  @serialize public readonly visible;
  @serialize public readonly x;
  @serialize public readonly y;
  @serialize public readonly width;
  @serialize public readonly height;
  @serialize public readonly content;

  constructor(id: string, type: string, style: string, cssClass: string, index: number,
              visible: boolean, x: number, y: number, width: number, height: number, content: ConfigElementContent) {
    this.id = id;
    this.type = type;
    this.style = style;
    this.cssClass = cssClass;
    this.index = index;
    this.visible = visible;
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.content = content;
  }
}
