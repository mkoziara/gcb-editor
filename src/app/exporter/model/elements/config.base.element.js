"use strict";
var ConfigBaseElement = (function () {
    function ConfigBaseElement(id, type, style, cssClass, index, visible, positionX, positionY, width, height) {
        this.id = id;
        this.type = type;
        this.style = style;
        this.cssClass = cssClass;
        this.index = index;
        this.visible = visible;
        this.positionX = positionX;
        this.positionY = positionY;
        this.width = width;
        this.height = height;
    }
    return ConfigBaseElement;
}());
exports.ConfigBaseElement = ConfigBaseElement;
