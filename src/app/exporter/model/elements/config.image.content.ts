import {serialize, inheritSerialization} from "cerialize";
import {ConfigElementContent} from "./config.element.content";

@inheritSerialization(ConfigElementContent)
export class ConfigImageContent extends ConfigElementContent {

  @serialize public readonly src;
  @serialize public readonly alt;

  constructor(src: string, alt: string) {
    super();

    this.src = src;
    this.alt = alt;
  }
}
