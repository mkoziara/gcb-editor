"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var config_base_element_1 = require("./config.base.element");
var ConfigImageElement = (function (_super) {
    __extends(ConfigImageElement, _super);
    function ConfigImageElement(id, type, style, cssClass, index, visible, positionX, positionY, width, height, imageSrc, alt) {
        _super.call(this, id, type, style, cssClass, index, visible, positionX, positionY, width, height);
        this.id = id;
        this.type = type;
        this.style = style;
        this.cssClass = cssClass;
        this.index = index;
        this.visible = visible;
        this.positionX = positionX;
        this.positionY = positionY;
        this.width = width;
        this.height = height;
        this.imageSrc = imageSrc;
        this.alt = alt;
    }
    return ConfigImageElement;
}(config_base_element_1.ConfigBaseElement));
exports.ConfigImageElement = ConfigImageElement;
