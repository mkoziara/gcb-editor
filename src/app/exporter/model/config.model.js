"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var cerialize_1 = require("cerialize");
var Config = (function () {
    function Config(name, title, engineName, engineSkin, engine) {
        this.name = name;
        this.title = title;
        this.engineName = engineName;
        this.engineSkin = engineSkin;
        this.engine = engine;
    }
    __decorate([
        cerialize_1.serialize
    ], Config.prototype, "name", void 0);
    __decorate([
        cerialize_1.serialize
    ], Config.prototype, "title", void 0);
    __decorate([
        cerialize_1.serialize
    ], Config.prototype, "engineName", void 0);
    __decorate([
        cerialize_1.serialize
    ], Config.prototype, "engineSkin", void 0);
    return Config;
}());
exports.Config = Config;
