import {ConfigBaseElement} from "./elements/config.base.element";
import {serialize, serializeAs} from "cerialize";

export class ConfigScene {

  @serialize public readonly name;
  @serialize public readonly background;
  @serializeAs('background-color') public readonly backgroundColor;
  @serialize public readonly elements;
  @serialize public readonly actions;
  @serialize public readonly logic;

  constructor(name: string,
              backgroundImage: string,
              backgroundColor: string,
              elements: Array<ConfigBaseElement>,
              actions: Array<any>,
              logic: Array<any>) {

    this.name = name;
    this.background = backgroundImage;
    this.backgroundColor = backgroundColor;
    this.elements = elements;
    this.actions = actions;
    this.logic = logic;
  }
}
