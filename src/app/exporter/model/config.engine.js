"use strict";
var ConfigEngine = (function () {
    function ConfigEngine(width, height, autoresize, appendScenes, globalElements, scenes) {
        this.width = width;
        this.height = height;
        this.autoresize = autoresize;
        this.appendScenes = appendScenes;
        this.globalElements = globalElements;
        this.scenes = scenes;
    }
    return ConfigEngine;
}());
exports.ConfigEngine = ConfigEngine;
