import {ConfigScene} from "./config.scene.model";
import {serialize} from "cerialize";

export class ConfigEngine {

  @serialize public readonly width: number;
  @serialize public readonly height: number;
  @serialize public readonly autoresize: boolean;
  @serialize public readonly appendScenes: boolean;
  @serialize public readonly global: Array<any>;
  @serialize public readonly scenes: Array<ConfigScene>;

  constructor(width: number,
              height: number,
              autoresize: boolean,
              appendScenes: boolean,
              globalElements: Array<any>,
              scenes: Array<ConfigScene>) {
    this.width = width;
    this.height = height;
    this.autoresize = autoresize;
    this.appendScenes = appendScenes;
    this.global = globalElements;
    this.scenes = scenes;
  }

}
