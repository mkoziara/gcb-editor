"use strict";
var ConfigScene = (function () {
    function ConfigScene(name, backgroundImage, backgroundColor, elements, actions, logic) {
        this.name = name;
        this.backgroundImage = backgroundImage;
        this.backgroundColor = backgroundColor;
        this.elements = elements;
        this.actions = actions;
        this.logic = logic;
    }
    return ConfigScene;
}());
exports.ConfigScene = ConfigScene;
