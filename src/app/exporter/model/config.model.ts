import {ConfigEngine} from "./config.engine";
import {serialize} from "cerialize";

export class Config {

  @serialize public readonly name;
  @serialize public readonly title;
  @serialize public readonly engineName;
  @serialize public readonly engineSkin;
  @serialize public readonly engine;

  constructor(name: string,
              title: string,
              engineName: string,
              engineSkin: string,
              engine: ConfigEngine) {
    this.name = name;
    this.title = title;
    this.engineName = engineName;
    this.engineSkin = engineSkin;
    this.engine = engine;

  }
}
