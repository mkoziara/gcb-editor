import {Injectable} from "@angular/core";
import {Simulation} from "../model/simulation.model";
import {SimulationMapper} from "./mapper/simulation.mapper";
import {Serialize} from "cerialize";

@Injectable()
export class ConfigExporter {

  public exportConfig(simulation: Simulation): void {

    let config = this.simulationMapper.mapToConfig(simulation);
    let json = Serialize(config);
    let configJson = JSON.stringify(json);

    window.open("data:text/json," + encodeURIComponent(configJson), "_blank");
  }

  constructor(private simulationMapper: SimulationMapper) {
  }
}
