import { GCBEditorPage } from './app.po';

describe('gcb-editor App', function() {
  let page: GCBEditorPage;

  beforeEach(() => {
    page = new GCBEditorPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
